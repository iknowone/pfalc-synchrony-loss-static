function [X] = replicate_oscillators(time_pts, X, pd, args)
% This function performs replication of a population of phase oscillators.
% Replication occurs after each complete cycle (i.e. when a 'mother'
% oscillator crosses the phase 0=1 after a complete cycle) at which point 
% args.rep_num 'daughter' oscillators are created and initialized with the 
% same phase as their mother and phase velocities drawn from a specified 
% distribution.
%
% - input - 
% X: TxN array encoding phases of N phase oscillators at T time points
% pd: distribution object from which to draw random phase velocities 
% args: argument structure specifying model and algorithm hyperparameters
%
% - output -
% X: TxM array encoding the phases of the population including oscillators 

% track each cells replication status
replication_flags = zeros(1,args.pop_size);
% track the generation to which each cell belongs
generation_ind = zeros(1,args.pop_size);
generation_count = 0;
while ~all(replication_flags)
    mother_generation_ind = find(generation_ind == generation_count);
    mothers = X(:, mother_generation_ind);
    replication_flags(mother_generation_ind) = 1;
    mothers_floor = floor(mothers);
    daughters_keep = cell(1, size(mothers, 2));
    for i=1:size(mothers, 2)
        % mother cells replicate after each full cycle
        replicate_indices = find(mothers_floor(2:end,i) > mothers_floor(1:end-1,i) & mothers_floor(1:end-1,i) >= 0) + 1;
        daughters = -1*ones(size(mothers,1), (args.rep_num-1)*length(replicate_indices));
        for j=1:length(replicate_indices)
            % daughters inherit mother's initial condition
            mother_ic = repmat(mothers(replicate_indices(j), i),[args.rep_num-1,1]);
            % daughters have phase velocities drawn from the current distribution
            daughters_phase_vels = random(pd, args.rep_num-1, 1);
            % compute daughter phase progression
            daughters(replicate_indices(j):end, (j-1)*(args.rep_num-1)+1:j*(args.rep_num-1))=(mother_ic + daughters_phase_vels*time_pts(1:end-replicate_indices(j)+1))';
            % add flags to track replication status and generation of new daughters
            generation_ind = cat(2, generation_ind, (generation_ind(mother_generation_ind(i))+j)*ones(1,args.rep_num-1));
            replication_flags = cat(2, replication_flags, zeros(1,args.rep_num-1));
        end
        % add daughters to current mother
        daughters_keep{i} = daughters;
    end
    % add daughters to full population
    X = cat(2, X, cell2mat(daughters_keep));
    
    disp(['finished generation ', num2str(generation_count)])
    
    % increment generation tracker
    generation_count = generation_count+1;
end