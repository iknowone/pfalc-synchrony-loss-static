function [args] = default_args(staging_data, cycle_length)
% default_args() constructs a default argument structure containing the 
% experimental data, the various run arguments, and genetic algorithm 
% options needed to specify the model and perform global optimization of
% model parameters. This ensures that the many arguments are trackable and 
% easy to identify to ensure reproducible results.

% if allow_rand = 1, populations generated at random
% if allow_rand = 0, same (random) population always used
args.allow_rand = 0;

% specify experimental staging data and simulated population size
args.staging_data = staging_data(:, 2:end);
args.time_pts = staging_data(:, 1)';

args.pop_size = 10000;

% specify parameter space bounds
args.init_phase_mean_lb = realmin;
args.init_phase_mean_ub = 1;

args.init_phase_sigma_lb = realmin;
args.init_phase_sigma_ub = .25;

args.phase_vel_sigma_lb = realmin;
args.phase_vel_sigma_ub = .15;

args.ring_troph_trans_lb = realmin;
args.ring_troph_trans_ub = 1;

args.troph_schiz_trans_lb = realmin;
args.troph_schiz_trans_ub = 1;

% By default, the assumed mean cycle length is fixed
args.mean_cycle_length_lb = cycle_length;
args.mean_cycle_length_ub = cycle_length;

args.nvars = 6;

% specify sample size used to generate simulated staging curves
args.sample_size = -1;  % default = entire population
args.num_samples = 1;  % default = entire population

% replication number
args.rep_num = 0;

% specify objective function and parameter contraints
args.obj_fxn = @pfalc_idc_obj_fxn;
args.obj_fxn_const = @pfalc_idc_obj_fxn_constraints;

% specify genetic algorithm solver options
args.ga_opts_pop_size = 5000;
args.ga_opts_xover_frac = 0.8;
args.ga_opts_max_gens = 10;
args.ga_opts_max_stall_gens = 50;
args.ga_opts_disp = 'iter';
args.ga_opts_para = true;
args.ga_opts_vect = false;
args.ga_opts_fxn_tol = 1e-8;
args.ga_opts_hybrid = {'patternsearch', optimoptions('patternsearch',...
                                                     'MeshTolerance', 1e-10,...
                                                     'FunctionTolerance', 1e-16,...
                                                     'MeshContractionFactor', 0.8,...
                                                     'MeshExpansionFactor', 4.0)};
end

