% This script quantifies and visualizes the impact of parasite replication on 
% staging curves using the continuous phase-oscillator model.

%%
% ----------------------------------------------------------------------
%  Load optimal param plots and compare staging curves with and without
%  replication with different values of phase progression rate 
%  variability 
% ----------------------------------------------------------------------
clear; clc;

% load the optimal parameters determined for each strain
load('../../data/pfalc/opt_params_struct.mat')

rng('default') % reset rng for reproducibility

strains = {'3d7', 'sa250', 'fvo', 'd6', 'hb3'};

% factors to scale optimal period-variability parameter (sigma^*) in phase
% oscillator models which include replication
sigma_factors = [1/2, 1, 2];

rep_simulation_struct = struct();
i=1;
for s=1:length(strains)
    strain = strains{s};
    
    % load staging data
    [staging_data, best_guess_cycle_length] = pfalc_idc_data(strain);
    
    % find optimal parameters for this strain and the best guess cycle length
    ind = strcmp({opt_params_struct.strain}, strain) & cellfun(@(x) x==best_guess_cycle_length, {opt_params_struct.best_cycle_length});
    
    % extract the original arguments and best fit parameters and 
    % simulate a large population for plotting
    opt_args = opt_params_struct(ind).args;
    opt_args.pop_size = 1000;
    rep_args = opt_args;
    opt_args.sample_size = 100;
    opt_args.num_samples = 10000;
    opt_params = [opt_params_struct(ind).best_mu_0,...
              opt_params_struct(ind).best_sigma_0,...
              opt_params_struct(ind).best_sigma,...
              opt_params_struct(ind).best_rt_phase,...
              opt_params_struct(ind).best_ts_phase,...
              opt_params_struct(ind).best_cycle_length];
    
    % simulate without replication
    [opt_staging_sim, opt_staging_sim_std] = pfalc_idc_simulation(opt_params, opt_args);
    
    % update arguments to include replication and simulate the same 
    % initial population of oscillators for comparison
    rep_args.rep_num = 4;
    
    % holding everything else fixed, update params with different choices 
    % of period variaiblity and simulate with replication
    for k=1:length(sigma_factors)
        sigma_factor = sigma_factors(k);
        % multiply the optimal sigma parameter by a scaling factor
        rep_params = opt_params;
        rep_params(3) = sigma_factor*rep_params(3);
        % simulate with replication
        rep_simulation_struct(i).strain = strain;
        rep_simulation_struct(i).analytic_period_cov = sqrt(tan(pi*rep_params(3))/(pi*rep_params(3))-1);
        rep_simulation_struct(i).sigma_factor = sigma_factor;
        rep_simulation_struct(i).rep_params = rep_params;
        [rep_staging_sim, ~] = pfalc_idc_simulation(rep_params, rep_args);
        rep_simulation_struct(i).rep_staging_sim = rep_staging_sim;
        rep_simulation_struct(i).opt_staging_sim = opt_staging_sim;
        rep_simulation_struct(i).opt_staging_sim_std = opt_staging_sim_std;
        rep_simulation_struct(i).rep_args = rep_args;
        
        i=i+1;
    end
    disp(['finished strain ', strain])
end


for s=1:length(strains)
    strain = strains{s};
    
    % load staging data
    [staging_data, best_guess_cycle_length] = pfalc_idc_data(strain);
    
    figure('color', 'w', 'units', 'normalized', 'outerposition', [0 0 1 1])
    colors = {[1,0,0],[0,1,0],[0,0,1]};
    stages = {'Ring','Trophozoite','Schizont'};
    for k=1:length(sigma_factors)
        sigma_factor = sigma_factors(k);
        
        ind = strcmp({rep_simulation_struct.strain}, strain) & cellfun(@(x) x==sigma_factor, {rep_simulation_struct.sigma_factor});   
        for j=1:3
            if sigma_factor == 1
                cinterp = colors{j};
            elseif sigma_factor < 1
                cinterp = (2*[0,0,0] + colors{j})/3;
            elseif sigma_factor > 1
                cinterp = (2*[1,1,1] + colors{j})/3;
            end
            subplot(3,1,j)
            hold on
            boundedline(rep_simulation_struct(ind).rep_args.time_pts, rep_simulation_struct(ind).opt_staging_sim(:,j), 2*rep_simulation_struct(ind).opt_staging_sim_std(:,j), 'alpha', 'cmap', colors{j});
            plot(rep_simulation_struct(ind).rep_args.time_pts, rep_simulation_struct(ind).rep_staging_sim(:,j), '--', 'LineWidth', 2, 'Color', cinterp);
            plot(staging_data(:,1), staging_data(:,j+1), '-', 'LineWidth', 2, 'Color', 'black')
            title([stages{j}])
            xlabel('Time [hours]')
            ylabel('Population %')
            axis tight
            ylim([0,1])
        end
    end
    if ~exist('../../figs/stage_curves_replication/', 'dir')
        mkdir('../../figs/stage_curves_replication/')
    end
    saveas(gcf, ['../../figs/stage_curves_replication/continuous_model_' strain '_replication.svg'])
    close
end


