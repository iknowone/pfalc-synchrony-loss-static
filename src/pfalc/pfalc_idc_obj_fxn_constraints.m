function [c, ceq] = pfalc_idc_obj_fxn_constraints(params)
% pfalc_idc_obj_fxn_constraints() defines constraints on the relationships 
% between parameters over which the objective function is defined.  In 
% particular, this function insists that the transition between the 
% ring and troph stage happens before the transition between the troph 
% and schiz stage
%
% - input -
% params: vector of parameter values
%          params(1) = initial population mean phase (mu_0)
%          params(2) = initial population phase shape parameter (sigma_0)
%          params(3) = population phase velocity shape parameter (sigma)
%          params(4) = ring to trophozoite stage transition phase
%                      (theta_{r,t})
%          params(5) = trophozoite to schizont stage transition phase 
%                      (theta_{t,s})
%          params(6) = mean cycle length in hours
% - output - 
% c: vector of formulas determining inequality constraints (f(x) <= 0)
% ceq: vector of expressions determining equality constraints (f(x) == 0)

% ensure that the stage transition phases abide by ring < troph < schiz
c = [params(4) - params(5)];
ceq = [];

end

