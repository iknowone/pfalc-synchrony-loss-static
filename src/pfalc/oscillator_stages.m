function [stages, stages_std] = oscillator_stages(X, stage_transitions, sample_size, num_samples)
% oscillator_stages() maps an oscillator's phase to a categorical stage and
% determines the percentage of the population in each stage at each time.
% Note: A phase of -1 at a given time is reserved for oscillators 
%       which are not yet "born", which occurs when modelling replication 
%       of oscillators. 
%
% - input - 
% time_pts: Tx1 vector of times points at which staging data was collected
% X: TxN vector encoding population phases
% stage_transitions: 1xm vector [t_1, ..., t_m] with 0 < t1 < ... < t_m < 1
%                    which determine phases separating the m+1, disjoint 
%                    categorical stages, e.g. [.3,.6] specifies a 3-stage
%                    oscillator with stage 1 = phase interval [0, .3)
%                                    stage 2 = phase interval [.3, .6)
%                                    stage 3 = phase interval [.6, 1)
% sample_size: integer specifying the size of a random sample of the 
%              oscillator population from which to build staging curves
%              default = -1, which specifies entire population
% num_samples: integer specifying the number of random samples of the 
%              oscillator population from which to build staging curves
%              default = 1.
%
% - output - 
% time_pts: Tx1 vector of times (same as input time_pts)
% stages: Tx(m+1) matrix containing percentage of oscillators in each stage 
%         at each time point, or the average percentage of oscillators in 
%         each stage at each time point if sampling from the population.
% stages_std: The standard deviation curve of the staging curve computed
%             by random sampling from the entire population.

[T, N] = size(X);
m = length(stage_transitions);

% reduce phases to [0,1)
X = mod_neg(X, 1);
stage_transitions = [0, stage_transitions, 1];

if sample_size == -1 || num_samples == 1
    % use the entire population of oscillators to generate the staging
    % determine percentage of oscillators in each stage
    stages = zeros(T, m+1);
    for i=1:m+1
        stages(:,i) = sum(stage_transitions(i) <= X & X < stage_transitions(i+1), 2)./sum(X>-1, 2); 
    end
    stages_std = [];
    
else
    % randomly sample from the entire population of oscillators to estimate
    % the distribution of staging curves
    stages = zeros(T, m+1);
    stages_std = zeros(T, m+1);
    stages_samples = zeros(num_samples, T, m+1);
    for sample=1:num_samples
        X_sample = X(:, randsample(N, sample_size));
        for i=1:m+1
            stages_samples(sample,:,i) = sum(stage_transitions(i) <= X_sample & X_sample < stage_transitions(i+1), 2)./sum(X_sample>-1,2);
        end
    end
    for i=1:m+1
        stages(:,i) = mean(stages_samples(:,:,i));
        stages_std(:,i) = std(stages_samples(:,:,i));
    end
end

end

