% This script generates plots of real and simulated staging curves
% using optimized parameters and parameters determined from the Leise data.
% It also creates supplemental figures 
%
% Run create_opt_params_table.m first to create and update
%              ../../data/pfalc/opt_params_struct.mat
% if it does not already exist.
%
% This script makes use of the function boundedline() available on the 
% MATLAB file exchange:
% https://www.mathworks.com/matlabcentral/fileexchange/27485-boundedline-m
clear
rng('default') % reset rng to ensure reproducibility

% update to local path of boundedline directory
boundedline_dir = '../../../../../matlab_functions/boundedline/';

% -----------------------------------------------------------------------
sample_size = 100;  % size of subsamples of oscillators to draw from population
num_samples = 100000;  % number of subsamples to draw from population

% add root directory to path
addpath(genpath('../../sync_loss'))

% added boundedline directory to path
addpath(genpath(boundedline_dir))

% load the precomputed data structures
load('../../data/pfalc/opt_params_struct.mat')
load('../../data/pfalc/cont_params_struct.mat')

% ========================================================================
%  Plots of staging curves (simulated, real, and control)
% ========================================================================

% compare optimal param simulations to control simulations at the inferred
% mean cycle lengths (control chosen at 1.75X circadian determined cycle
% length variaiblity shape parameter)
opt_param_inds = [2,5,8,11,14];
cont_param_inds = [3,6,9,12,15];
c = 1;
figure('color', 'w', 'units', 'normalized', 'outerposition', [1 0 1 1])
for k=1:length(opt_param_inds)
    i1 = opt_param_inds(k);
    i2 = cont_param_inds(k);
    strain = opt_params_struct(i1).strain;
    cycle_length = opt_params_struct(i1).best_cycle_length;
    
    % extract and update run arguments
    args = opt_params_struct(i1).args;
    args.sample_size = sample_size;
    args.num_samples = num_samples;
    
    args2 = cont_params_struct(i2).args;
    args2.sample_size = sample_size;
    args2.num_samples = num_samples;
    
    % reconstruct parameter vectors
    opt_param = [opt_params_struct(i1).best_mu_0;
        opt_params_struct(i1).best_sigma_0;
        opt_params_struct(i1).best_sigma;
        opt_params_struct(i1).best_rt_phase;
        opt_params_struct(i1).best_ts_phase;
        opt_params_struct(i1).best_cycle_length]';
    opt_cov = opt_params_struct(i1).analytic_period_cov;
    cont_param = [cont_params_struct(i2).best_mu_0;
        cont_params_struct(i2).best_sigma_0;
        cont_params_struct(i2).best_sigma;
        cont_params_struct(i2).best_rt_phase;
        cont_params_struct(i2).best_ts_phase;
        cont_params_struct(i2).best_cycle_length]';
    cont_cov = cont_params_struct(i2).analytic_period_cov;
    
    % ------------------------------------------------------------------------
    % Plot staging curves for real data and optimal parameters
    % subsampling 'sample_size' oscillators 'num_samples' times from the
    % entire population to generate mean and standard deviation staging curves
    [opt_staging_sim, opt_staging_sim_std] = pfalc_idc_simulation(opt_param, args);
    [cont_staging_sim, cont_staging_sim_std] = pfalc_idc_simulation(cont_param, args2);
    
    colors = {[232,134,99]/255,[131,181,208]/255,[188,222,206]/255};
    stages = {'Ring','Trophozoite','Schizont'};
    for j=1:3
        subplot(5,3,c)
        p = plot(args.time_pts, args.staging_data(:,j), '--', 'LineWidth', 2, 'Color', colors{j});
        hold on
        [hl, ~] = boundedline(args.time_pts, opt_staging_sim(:,j), 2*opt_staging_sim_std(:,j), 'alpha', 'cmap', colors{j});
        [~, hp] = boundedline(args2.time_pts, cont_staging_sim(:,j), 2*cont_staging_sim_std(:,j), 'alpha', 'cmap', 'k');
        title([stages{j}, ' | CoVs: optimum-', num2str(opt_cov), ', control-' num2str(cont_cov)])
        xlabel('time [hours]')
        c = c+1;
        axis tight
        ylim([0,1])
        yticks([0,.5,1])
        xticks([0, cycle_length])
        pbaspect([2.5 1 1]);
        hold off
    end
end
    
if ~exist('../../figs/stage_curves_comp/', 'dir')
   mkdir('../../figs/stage_curves_comp/')
end
saveas(gcf, ['../../figs/stage_curves_comp/pfalc_opt_vs_cont_stage_curves.svg'])
close

%%
% ========================================================================
%  Plot of coefficient of variation of periods
% ========================================================================
strains = {'3d7', 'sa250', 'fvo', 'd6', 'hb3'};
strain_colors = {'r', 'g', 'b', 'y', 'c'};

figure('color', 'w', 'units', 'normalized', 'outerposition', [0 0 1 1])
hold on
for i=1:length(strains)
    strain = strains{i};
    % extract optimum for each strain and each choice of cycle length
    all_best_period_covs = [opt_params_struct(strcmp(strain, {opt_params_struct.strain})).analytic_period_cov];
    cycle_lengths = [opt_params_struct(strcmp(strain, {opt_params_struct.strain})).best_cycle_length]; 

    % ensure mean cycle lengths are properly labelled for each strain
    [~, ind] = sort(cycle_lengths);
    all_best_period_covs = all_best_period_covs(ind);
    cycle_lengths = cycle_lengths(ind);
    
    line([0,0.15], [6-i, 6-i], 'Color', [120, 120, 120]/255, 'LineStyle','--', 'LineWidth', 1)
    text(-.002, 6-i , strain, 'HorizontalAlignment', 'right')
    
    % plot the mean period covs for different assumed mean period lengths
    scatter(all_best_period_covs(1), 6-i, 100, strain_colors{i}, '<', 'filled', 'MarkerEdgeColor', 'k')
    scatter(all_best_period_covs(2), 6-i, 100, strain_colors{i}, 'd', 'filled', 'MarkerEdgeColor', 'k')
    scatter(all_best_period_covs(3), 6-i, 100, strain_colors{i}, '>', 'filled', 'MarkerEdgeColor', 'k')
end
line([opt_params_struct(1).leise_data_p2p_time_cov, opt_params_struct(1).leise_data_p2p_time_cov], [0, 6], 'Color', 'k', 'LineStyle','-', 'LineWidth', 1)
text(opt_params_struct(1).leise_data_p2p_time_cov, 6.25, 'Circadian Periods', 'HorizontalAlignment', 'center')
hold off

xlim([0,0.15])
xlabel('Coefficient of Variation of Period [cycles]')
set(gca,'xtick',[0,.025,.05,.075,.1,.125, 1.5])
ylim([0,6])
set(gca,'ytick',[])
pbaspect([5 1 1])

if ~exist('../../figs/pfalc_cycle_cov/', 'dir')
    mkdir('../../figs/pfalc_cycle_cov/')
end
saveas(gcf, '../../figs/pfalc_cycle_cov/pfalc_cycle_cov.svg')
close

%%
% ========================================================================
%  Bar chart version of coefficient of variation of periods
% ========================================================================
strains = {'3d7', 'sa250', 'fvo', 'd6', 'hb3'};
strain_colors = {'r', 'g', 'b', 'y', 'c'};

figure('color', 'w', 'units', 'normalized', 'outerposition', [0 0 1 1])
opt_param_inds = [2,5,8,11,14];
all_best_period_covs = zeros(length(opt_param_inds),3);
all_cycle_lengths = zeros(length(opt_param_inds),3);
for k=1:length(opt_param_inds)
    i = opt_param_inds(k);
    strain = strains{k};
    % extract optimum for each strain and each choice of cycle length
    period_covs = [opt_params_struct(strcmp(strain, {opt_params_struct.strain})).analytic_period_cov];
    cycle_lengths = [opt_params_struct(strcmp(strain, {opt_params_struct.strain})).best_cycle_length]; 
    
    % ensure mean cycle lengths are properly labelled for each strain
    [~, ind] = sort(cycle_lengths);
    period_covs = period_covs(ind);
    all_cycle_lengths(k,:) = cycle_lengths(ind);
    
    cycle_length = opt_params_struct(i).best_cycle_length;
    
    % extract optimum for each strain and each choice of cycle length
    all_best_period_covs(k,:) = period_covs;
end

hb = bar(all_best_period_covs);
hold on
for i=1:size(all_cycle_lengths,1)
    for ib = 1:numel(hb)
        x = hb(ib).XData + hb(ib).XOffset;
        text(x(i), all_best_period_covs(i,ib), num2str(all_cycle_lengths(i,ib)),...
             'HorizontalAlignment','center',...
             'VerticalAlignment','bottom')
    end
    
end
set(gca,'xticklabel', strains);
line(xlim, [opt_params_struct(1).leise_data_p2p_time_cov, opt_params_struct(1).leise_data_p2p_time_cov], 'Color', 'k', 'LineStyle','-', 'LineWidth', 2)
text(max(xlim), opt_params_struct(1).leise_data_p2p_time_cov, 'Circadian Periods', 'HorizontalAlignment', 'right', 'VerticalAlignment','bottom')
ylabel('Coefficient of Variation of Periods [cycles]')
hold off

if ~exist('../../figs/pfalc_cycle_cov/', 'dir')
    mkdir('../../figs/pfalc_cycle_cov/')
end
saveas(gcf, '../../figs/pfalc_cycle_cov/pfalc_cycle_cov_bar.svg')
close