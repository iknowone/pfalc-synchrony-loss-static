% This script generates the strain-specific optimal parameter and
% coefficient of variation table, assuming fixed intrinsic cycle lengths,
% and no replication. It stores the results in a data structure for 
% analysis and plotting.

% An average intrinsic cycle length, per, was estimated from microscopy and 
% RNA transcript data for each strain. Optimal parameters are computed for 
% each per-1, per, and per+1.
clear;
rng('default')

strains = {'3d7', 'sa250', 'fvo', 'd6', 'hb3'};
per_pm = [-1,0,1];

% -----------------------------------------------------------------------
% add root directory to path
addpath(genpath('../../sync_loss'))

opt_params_struct = struct();

i = 1;
for s=3:length(strains)
    strain = strains{s};
    % load staging data
    [staging_data, best_guess_cycle_length] = pfalc_idc_data(strain);
    
    for p=1:length(per_pm)
        cycle_length = best_guess_cycle_length + per_pm(p);
        
        % compute optimal parameters for this strain and this 
        % fixed cycle length
        opt_params_struct(i).strain = strain;

        args = default_args(staging_data, cycle_length);
        [opt_param, se_at_opt_param, exit_flag, analytic_period_cov, leise_data_p2p_time_cov, leise_data_p2p_sigma] = pfalc_idc_optimal_params(args);
        
        % store results in the parameter data structure
        opt_params_struct(i).best_cycle_length = cycle_length;
        opt_params_struct(i).analytic_period_cov = analytic_period_cov;
        opt_params_struct(i).leise_data_p2p_time_cov = leise_data_p2p_time_cov;
        opt_params_struct(i).leise_data_p2p_sigma = leise_data_p2p_sigma;
        opt_params_struct(i).best_sigma = opt_param(3);
        opt_params_struct(i).best_mu = log(pi*opt_param(3)/sin(pi*opt_param(3)));
        opt_params_struct(i).best_mu_0 = opt_param(1);
        opt_params_struct(i).best_sigma_0 = opt_param(2);
        opt_params_struct(i).best_rt_phase = opt_param(4);
        opt_params_struct(i).best_ts_phase = opt_param(5);
        
        opt_params_struct(i).se_at_opt_param = se_at_opt_param;
        
        opt_params_struct(i).args = args;
        opt_params_struct(i).exit_flag = exit_flag;
        
        i=i+1;
        
        % save the updated data structure
        save('../../data/pfalc/opt_params_struct.mat', 'opt_params_struct')
    end
end