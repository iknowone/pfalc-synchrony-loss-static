% This script generates the strain-specific control parameters and
% coefficient of variation table, storing the modelling results in a 
% data structure for analysis and plotting. Here we insist on larger 
% variability in cycle period than is observed by parameter optimization 
% or in the Leise 2012, circadian data. Specifically the minimum
% value of is taken to be X*p2p_sigma, for X = 1.25, 1.5, 1.75 and where 
% p2p_sigma is the value of the shape parameter for the best fit 
% log-logistic distribution to the Leise 2012 peak-to-peak times.

% An average intrinsic cycle length, per, was estimated from microscopy and 
% transcript data for each strain. Optimal parameters are computed for each
% per-1, per, and per+1.
clear;
rng('default')

strains = {'3d7', 'sa250', 'fvo', 'd6', 'hb3'};
per_pm = 0;
sigma_lbs = [0.0580060770581587, 0.06960729246979050, 0.08120850788142224];
% 1.25, 1.5, 1.75 times sigma for Leise 2102 peak-to-peak data

% -----------------------------------------------------------------------
% add root directory to path
addpath(genpath('../../sync_loss'))

cont_params_struct = struct();

i = 1;
for s=1:length(strains)
    strain = strains{s};
    % load staging data
    [staging_data, best_guess_cycle_length] = pfalc_idc_data(strain);
    for b=1:length(sigma_lbs)
        % set lower bound on phase velocity sigma model parameter
        phase_vel_sigma_lb = sigma_lbs(b);
        
        for p=1:length(per_pm)
            cycle_length = best_guess_cycle_length + per_pm(p);
            
            % compute optimal parameters for this strain and this 
            % fixed cycle length with a prescribed lower bound
            % on the period variability shape parameter
            cont_params_struct(i).strain = strain;
            
            % load default arguments and update with new minimum phase 
            % velocity sigma lower bound
            args = default_args(staging_data, cycle_length);
            args.phase_vel_sigma_lb = phase_vel_sigma_lb;
            
            cont_params_struct(i).best_cycle_length = cycle_length;
            [cont_param, se_at_cont_param, exit_flag, analytic_period_cov, leise_data_p2p_time_cov, leise_data_p2p_sigma] = pfalc_idc_optimal_params(args);
            
            % store results in data structure
            cont_params_struct(i).min_sigma = phase_vel_sigma_lb;
            cont_params_struct(i).best_sigma = cont_param(3);
            cont_params_struct(i).best_mu = log(pi*cont_param(3)/sin(pi*cont_param(3)));
            cont_params_struct(i).best_mu_0 = cont_param(1);
            cont_params_struct(i).best_sigma_0 = cont_param(2);
            cont_params_struct(i).best_rt_phase = cont_param(4);
            cont_params_struct(i).best_ts_phase = cont_param(5);
            
            cont_params_struct(i).analytic_period_cov = analytic_period_cov;
            cont_params_struct(i).leise_data_p2p_time_cov = leise_data_p2p_time_cov;
            cont_params_struct(i).leise_data_p2p_sigma = leise_data_p2p_sigma;
            
            cont_params_struct(i).se_at_opt_param = se_at_cont_param;
            
            cont_params_struct(i).args = args;
            cont_params_struct(i).exit_flag = exit_flag;
            
            i=i+1;
            
            % save updated data structure
            save('../../data/pfalc/cont_params_struct.mat', 'cont_params_struct');
        end
    end
end





