function [x] = mod_neg(x,k)
% mod_neg() computes mod(x,k) only on positive entries of x
% 
% - input -
% x - arbitrary array
% k - real float
%
% - output -
% array of size(x) with positive entries reduced mod k

    for i=1:size(x,1)
        for j=1:size(x,2)
            if x(i,j) > 0
                x(i,j) = mod(x(i,j),k);
            end
        end
    end
end

