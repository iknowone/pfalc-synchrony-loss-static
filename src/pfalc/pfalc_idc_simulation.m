function [staging_sim, staging_sim_std] = pfalc_idc_simulation(params, args)
% pfalc_idc_simulation() performs a numerical simulation of a population of 
% independent generic phase oscillators representing the phases of members 
% of a population of P. falciparum parasites during the intraerythrocytic 
% developmental cycle (idc)
%
% - input -
% params: vector of parameter values
%          params(1) = initial population mean phase (mu_0)
%          params(2) = initial population phase shape parameter (sigma_0)
%          params(3) = population phase velocity shape parameter (sigma)
%          params(4) = ring to trophozoite stage transition phase
%                      (theta_{r,t})
%          params(5) = trophozoite to schizont stage transition phase 
%                      (theta_{t,s})
%          params(6) = mean cycle length in hours
% args: argument structure specifying model and algorithm hyperparameters
%
% - inputs extracted from arguments structure -
% pop_size: size of population
% time_pts: Tx1 vector of times points at which staging data was collected
% allow_rand: boolean flag indicating if random population should be fixed
% sample_size: integer specifying the size of a random sample of the 
%              oscillator population from which to build staging curves
%              default = -1, which specifies entire population
% num_samples: integer specifying the number of random samples of the 
%              oscillator population from which to build staging curves
%              default = 1.
% rep_factor: integer specifying number of replications per cycle
%
% - output - 
% staging_sim: Tx3 matrix of simulated staging information

% (1) given the current parameter vector, generate a random population of 
%     oscillators and simulate phase evolution

% optionally reset random seed to produce same initial condition
if ~args.allow_rand
    rng(1)
end

% rescale time points to units of # of cycles
time_pts = args.time_pts / params(6);

% initial parasite phases are assumed to follow a wrapped normal
% distribution
phase_ic = mod(params(2) * randn(args.pop_size,1) + params(1), 1);

% phase velocities are assumed to follow a log logistic distribution with
% shape parameter phase_vel_sigma and scale parameter 
% mu=log(pi*phase_vel_sigma/sin(pi*phase_vel_sigma))
pd = makedist('loglogistic', 'mu', log(pi*params(3)/sin(pi*params(3))), 'sigma', params(3));
phase_vels = random(pd, args.pop_size, 1);

% (2) simulate population of coupled oscillators to generate staging
%     information over time
% compute oscillator population phase progession
X = (phase_ic + phase_vels * time_pts)';

% optionally perform replication
if args.rep_num > 0
    X = replicate_oscillators(time_pts, X, pd, args);
end

% (3) generate staging curves based on current stage fraction parameters
[staging_sim, staging_sim_std] = oscillator_stages(X, [params(4), params(5)], args.sample_size, args.num_samples);

end