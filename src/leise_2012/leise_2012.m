% Reanalysis of Leise 2012 (https://doi.org/10.1371/journal.pone.0033334) 
% fibroblast PER2::LUC bioluminescence data

clear; clc;
%% load original data
filename = '../../data/leise_2012/leise_2012.csv';
delimiter = ',';
start_row = 4;
file = fopen(filename, 'r', 'n', 'UTF-8');
textscan(file, '%[^\n\r]', start_row-1, 'WhiteSpace', '', 'ReturnOnError', false, 'EndOfLine', '\r\n');
data = textscan(file, ['%d%D%D' repmat('%f',1,82) '%[^\n\r]'], 'Delimiter', delimiter, 'ReturnOnError', false);
fclose(file);
% store time series data in structure indexed by cell number
data_struct = struct('time', [], 'lumi', [], 'lumiden', [], 'per_range', [],...
                     'peaks', [], 'spur_peaks', [], 'p2p_times', []);

% cells 1-30
for i=6:35
    time = data{2};
    lumi = data{i};
    time = time(~isnan(lumi));
    lumi = lumi(~isnan(lumi));
    data_struct(i-5).time = time;
    data_struct(i-5).lumi = lumi;
end
% cells 31-80
for i=36:85
    time = data{3};
    lumi = data{i};
    time = time(~isnan(lumi));
    lumi = lumi(~isnan(lumi));
    data_struct(i-5).time = time;
    data_struct(i-5).lumi = lumi;
end

% cleanup
clear data delimiter start_row i filename file ans 

%% perform denoising using stationary discrete wavelet transform 
wname = 'db12';
level = 4;
div = 2^(level+3);
for i=1:80
    % estimate retained frequency content retained
    dt = hours(mean(diff(data_struct(i).time)));
    fs = 1 / dt;
    fn = fs/2;
    fq = scal2frq(2^level, wname, dt);
    data_struct(i).per_range = [1/scal2frq(2^level, wname, dt), ...
                                1/scal2frq(2^(level+2), wname, dt)];
    
    sig = data_struct(i).lumi;
    % extend signal to length equal to smallest integer divisible by
    % 2^(level+3)
    ext_len = (floor(length(sig)/div)+1)*div - length(sig);
    if mod(ext_len, 2) == 0
        sig_ext = wextend('1d', 'sp0', sig, ext_len/2);
    else
        sig_ext = wextend('1d', 'sp0', sig, ceil(ext_len/2), 'l');
        sig_ext = wextend('1d', 'sp0', sig_ext, ceil(ext_len/2) - 1, 'r');
    end
    
    % compute level+3 details and approximations
    [swa, swd] = swt(sig_ext, level+3, wname);
    % zero detail coefficients up to level
    swd(1:level, :) = 0;
    swd(level+3, :) = 0;
    
    % construct and save denoised signal
    sig_den = iswt(swa, swd , wname);
    data_struct(i).lumiden = sig_den(ceil(ext_len/2)+1:ceil(ext_len/2) + length(sig))';
    
    % compute peak-to-peak times   
    [p2p_times, peaks, spur_peaks] = compute_p2p_times(data_struct(i).time, ...
                                                       data_struct(i).lumiden, ...
                                                       3, .25);
    data_struct(i).p2p_times = p2p_times;
    data_struct(i).peaks = peaks;
    data_struct(i).spur_peaks = spur_peaks;
end


%% Plot spurious peaks example
cell_num = 34;

figure('color','w', 'units', 'normalized','position', [0,0,1,.5])
hold on
% convert timestamps to hours since first time point
timepoints = 24*(datenum(data_struct(cell_num).time) - datenum(data_struct(cell_num).time(1)));
% plot original time series
plot(timepoints, data_struct(cell_num).lumi, 'linewidth', 1, 'color', [120,120,120]/255)
% plot denoised time series
plot(timepoints, data_struct(cell_num).lumiden, 'linewidth', 2, 'color', [0, 0, 255]/255)
% plot all peaks
scatter(timepoints(data_struct(cell_num).peaks), ...
        data_struct(cell_num).lumiden(data_struct(cell_num).peaks), 50, 'blue', 'filled')
% plot spurious peaks
    scatter(timepoints(data_struct(cell_num).spur_peaks), ...
            data_struct(cell_num).lumiden(data_struct(cell_num).spur_peaks), 50, ...
            'red', 'filled')
axis tight
xlabel('time [hours]')
ylabel('bioluminescence')

if ~exist('../../figs/leise_2012/', 'dir')
    mkdir('../../figs/leise_2012/')
end
saveas(gcf, '../../figs/leise_2012/leise_2012_spurious_peaks.svg')
close

num_peaks = length(vertcat(data_struct.peaks));
num_spur_peaks = length(vertcat(data_struct.spur_peaks));
display(['The total number of peaks in the data: ' num2str(num_peaks)])
display(['The total number of spurious peaks in the data: ' num2str(num_spur_peaks)])

%% analyze peak-to-peak times
pop_p2p_times = 24 * datenum(vertcat(data_struct.p2p_times));

% fit lognormal distribution of population peak-to-peak times
figure('color','w', 'units', 'normalized','position', [0,0,1,.5])

subplot(1,2,1)
histogram(pop_p2p_times, 40, 'Normalization', 'pdf', 'edgecolor', 'none')
xlabel('Period [hours]')
ylabel('Probability')

subplot(1,2,2)
pd = fitdist(pop_p2p_times, 'loglogistic');
qqplot(pop_p2p_times, pd)

saveas(gcf, '../../figs/leise_2012/leise_2012_p2p_distribution.svg')
close

lldist = fitdist(pop_p2p_times/mean(pop_p2p_times), 'loglogistic');
lldist.negloglik

data_p2p_times_cov = std(pop_p2p_times)/mean(pop_p2p_times);
analytic_p2p_times_cov = lldist.std/lldist.mean;

display(['The empirical CoV of circadian periods: ' num2str(data_p2p_times_cov)])
display(['The loglogistic analytic CoV of circadian periods: ' num2str(analytic_p2p_times_cov)])

% Compute deviation of intrinsic (average) period across population as a
% function of the number of peak-to-peak intervals used to estimate the
% intrinsic period (audit of Fig. 2C in Leise 2012)
std_int_per = zeros(30,1);
for num_p2p = 1:30
   int_per = zeros(1,80);
   for i=1:80
       temp_p2p_times = 24*datenum(data_struct(i).p2p_times);
       temp_p2p_times = temp_p2p_times(1:min(length(temp_p2p_times),num_p2p));
       int_per(i) = mean(temp_p2p_times);
   end
   std_int_per(num_p2p) = std(int_per);
end
figure('color', 'w')
plot(1:30, std_int_per, 'color', 'k', 'linewidth', 2)
title('Period (h)')
xlabel('# cycles')
ylabel('SD (h)')
ylim([0,2.5])
axis square